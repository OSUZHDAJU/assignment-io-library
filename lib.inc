section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        mov cl, [rdi + rax]
        test cl, cl
        jz .exit
        inc rax
        jmp .loop
    .exit:        
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov rsi, rdi
    call string_length
    mov rdx, rax
    mov rdi, 1
    mov rax, 1
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0x0A

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rdx, 1
    mov rdi, 1
    mov rax, 1
    syscall
    pop rdi
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rdi, 10
    dec rsp
    mov byte[rsp], 0
    .save_num:
        xor rdx, rdx
        div rdi
        add rdx, '0'
        dec rsp
        mov [rsp], dl
        test rax, rax
        jnz .save_num
    mov rdi, rsp
    call print_string
    add rsp, rax
    inc rsp
    ret    

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov rax, 1
    .while:
        mov dl, [rdi]
        cmp dl, [rsi]
        jnz .unequal
        test dl, dl
        jz .finish
        inc rdi
        inc rsi
        jmp .while
    .unequal:
        xor rax, rax
    .finish:
        ret        

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    dec rsp
    xor rax, rax
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 1
    syscall
    inc rsp
    test rax, rax
    jz .finish
    mov al, [rsp - 1]
    .finish: ret 

read_char_and_join_spaces:
    call read_char
    cmp rax, 9
    je .space
    cmp rax, 10
    je .space
    ret
    .space:
    mov rax, 32
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push rsi
    test rsi, rsi
    jz .err
    push 0
    push rdi
    .skip_spaces:
        call read_char_and_join_spaces
        test rax, rax
        jz .finish
        cmp rax, 32
        je .skip_spaces 
    pop rdi
    pop rdx
    mov [rdi], al
    inc rdx
    cmp rdx, [rsp]
    je .err
    .read:
        push rdx
        push rdi
        call read_char_and_join_spaces
        test rax, rax
        jz .finish
        cmp rax, 32
        je .finish
        pop rdi
        pop rdx
        mov [rdi + rdx], al
        inc rdx
        cmp rdx, [rsp]
        jne .read
    .err:
        pop rsi
        xor rax, rax
        ret
    .finish:
        pop rdi
        pop rdx
        pop rsi
        mov byte[rdi + rdx], 0
        mov rax, rdi
        ret

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
        xor rax, rax
        mov al, [rdi]
        cmp al, '0'
        jl .err
        cmp al, '9'
        jg .err
        mov rdx, 1
        sub al, '0'
        mov rsi, 10
        xor rcx, rcx
    .while:
        mov cl, [rdi + rdx]
        cmp cl, '0'
        jl .finish
        cmp cl, '9'
        jg .finish
        inc rdx
        push rdx
        mul rsi
        pop rdx
        sub cl, '0'
        add rax, rcx
        jmp .while
    .err:    xor rdx, rdx
    .finish: ret





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
        cmp byte[rdi], '-'
        jz .negative
        call parse_uint
        ret
    .negative:
        inc rdi
        call parse_uint
        test rdx, rdx
        jz .err
        inc rdx
        neg rax
        ret
    .err:
        xor rax, rax    
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    call string_length
    cmp rax, rdx
    jg .err
    xor rax, rax
    .while:
        cmp rax, rdx
        je .err
        mov cl, [rdi + rax]
        mov [rsi + rax], cl
        test cl, cl
        jz .finish
        inc rax
        jmp .while   
    .err:    xor rax, rax
    .finish: ret
